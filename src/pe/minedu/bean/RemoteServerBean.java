package pe.minedu.bean;

import java.io.Serializable;

public class RemoteServerBean implements Serializable{
    //Datos para ejecutar comandos
    private String accion;
    private String documento;
    
    //Datos para crear carpeta de estudiante
    private String codigoUsuario;
    private String codigoClase;
    private String perfil;
    
    //Enviar mensajes a los alumnos
    private String nombreDocente;
    private String mensaje;

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getCodigoClase() {
        return codigoClase;
    }

    public void setCodigoClase(String codigoClase) {
        this.codigoClase = codigoClase;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getNombreDocente() {
        return nombreDocente;
    }

    public void setNombreDocente(String nombreDocente) {
        this.nombreDocente = nombreDocente;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "RemoteServerBean{" + "accion=" + accion + ", documento=" + documento + ", codigoUsuario=" + codigoUsuario + ", codigoClase=" + codigoClase + ", perfil=" + perfil + ", nombreDocente=" + nombreDocente + ", mensaje=" + mensaje + '}';
    }
    
}
