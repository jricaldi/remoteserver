package pe.minedu.main;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.UIManager;

public class RemoteServerWindow extends JFrame{
    TrayIcon trayIcon;
    SystemTray tray;
    RemoteServerWindow(){
        super("Minedu Demon");
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(Exception e){
            System.out.println("No es posible setear LookAndFeel");
        }
        if(SystemTray.isSupported()){
            tray=SystemTray.getSystemTray();

            Image image=Toolkit.getDefaultToolkit().getImage("/pe/minedu/img/icono.png");
            ActionListener exitListener=new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.out.println("Cerrando....");
                    System.exit(0);
                }
            };
            PopupMenu popup=new PopupMenu();
            MenuItem defaultItem=new MenuItem("Cerrar");
            defaultItem.addActionListener(exitListener);
            popup.add(defaultItem);
            trayIcon=new TrayIcon(image, "Minedu Demon", popup);
            trayIcon.setImageAutoSize(true);
        }else{
            System.out.println("system tray no soportado");
        }
        addWindowStateListener(new WindowStateListener() {
            public void windowStateChanged(WindowEvent e) {
                if(e.getNewState()==ICONIFIED){
                    try {
                        tray.add(trayIcon);
                        setVisible(false);
                    } catch (AWTException ex) {
                        System.out.println("No es posible agregar un tray");
                    }
                }
        if(e.getNewState()==7){
                    try{
            tray.add(trayIcon);
            setVisible(false);
            }catch(AWTException ex){
            System.out.println("No es posible agregar un system tray");
        }
            }
        if(e.getNewState()==MAXIMIZED_BOTH){
                    tray.remove(trayIcon);
                    setVisible(true);
                }
                if(e.getNewState()==NORMAL){
                    tray.remove(trayIcon);
                    setVisible(true);
                }
            }
        });
        setVisible(true);
        setState(java.awt.Frame.ICONIFIED);
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        RemoteServer remoteServer = new RemoteServer();
        remoteServer.start();
    }
    public static void main(String[] args){
        new RemoteServerWindow();
    }
}