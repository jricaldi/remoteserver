
package pe.minedu.main;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import pe.minedu.bean.RemoteServerBean;
import pe.minedu.service.AbrirArchivoService;
import pe.minedu.service.EnviarMensajeService;
import pe.minedu.service.EquipoRemotoService;
import pe.minedu.util.Constantes;

public class RemoteServer extends Thread{

    @Override
    public void run() {
        try {
            while (true) {
                ServerSocket server = new ServerSocket(5494);
                Socket client = server.accept();
                ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
                RemoteServerBean remoteServerBean = (RemoteServerBean) ois.readObject();
                if (remoteServerBean.getAccion().equals(Constantes.COMANDO_SCREENSHOOT)) {
                    Robot robot = new Robot();
                    BufferedImage screen;
                    Rectangle size = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
                    screen = robot.createScreenCapture(size);
                    int[] rgbData = new int[(int) (size.getWidth() * size.getHeight())];
                    screen.getRGB(0, 0, (int) size.getWidth(), (int) size.getHeight(), rgbData, 0, (int) size.getWidth());
                    OutputStream baseOut = client.getOutputStream();
                    ObjectOutputStream out = new ObjectOutputStream(baseOut);
                    //ImageIO.write(screen, "png", new File("orig_screen.png"));
                    out.writeObject(size);
                    for (int x = 0; x < rgbData.length; x++) {
                        out.writeInt(rgbData[x]);
                    }
                    out.flush();
                    server.close();
                    client.close();
                    out.close();
                    baseOut.close();
                } else if (remoteServerBean.getAccion().equals(Constantes.COMANDO_BLOQUEAR_ESCRITORIO)) {
                    EquipoRemotoService.bloquearEquipo();
                    server.close();
                    client.close();
                } else if (remoteServerBean.getAccion().equals(Constantes.COMANDO_DESBLOQUEAR_ESCRITORIO)) {
                    EquipoRemotoService.desbloquearEquipo();
                    server.close();
                    client.close();
                } else if(remoteServerBean.getAccion().equals(Constantes.COMANDO_ABRIR_ARCHIVO)){
                    AbrirArchivoService abrirArchivoService = new AbrirArchivoService();
                    abrirArchivoService.abrirArchivoSesion(remoteServerBean);
                    server.close();
                    client.close();
                } else if(remoteServerBean.getAccion().equals(Constantes.COMANDO_ABRIR_ARCHIVO_NUEVO)){
                    AbrirArchivoService abrirArchivoService = new AbrirArchivoService();
                    abrirArchivoService.abrirArchivoNuevo(remoteServerBean);
                    server.close();
                    client.close();
                } else if(remoteServerBean.getAccion().equals(Constantes.COMANDO_ENVIAR_MENSAJE)){
                    EnviarMensajeService.mostarMensaje(remoteServerBean.getMensaje(), remoteServerBean.getNombreDocente());
                    server.close();
                    client.close();
                }  else if(remoteServerBean.getAccion().equals(Constantes.COMANDO_ABRIR_BROWSER)){
                    // falta codigo
                    server.close();
                    client.close();
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            System.out.println("ex " + ex.getMessage());
            ex.printStackTrace();
        }
    }
    
}
