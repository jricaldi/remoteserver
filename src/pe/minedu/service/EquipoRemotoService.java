
package pe.minedu.service;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EquipoRemotoService implements Serializable{

    private static void releaseKeys(Robot robot) {
        robot.keyRelease(17);
        robot.keyRelease(18);
        robot.keyRelease(127);
        robot.keyRelease(524);
        robot.keyRelease(9);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.keyRelease(KeyEvent.VK_TAB);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {

        }
    }

    private static void kill(String string) {
        try {
            Runtime.getRuntime().exec("taskkill /F /IM " + string).waitFor();
        } catch (Exception e) {
        }
    }

    public static void bloquearEquipo() {
        try {
            kill("explorer.exe"); // Kill explorer
            Robot robot = new Robot();
            int i = 0;
            sleep(30L);
            releaseKeys(robot);
            sleep(15L);
            if (i++ % 10 == 0) {
//                kill("taskmgr.exe");
            }
            releaseKeys(robot);
        } catch (AWTException ex) {
            Logger.getLogger(EquipoRemotoService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void desbloquearEquipo() {
        try {
//            Runtime.getRuntime().exec("explorer.exe");
            ProcessBuilder pb = new ProcessBuilder("cmd", "/C", "start", "/B", "/NORMAL", "explorer.exe");
            pb.start();
        } catch (IOException ex) {
            Logger.getLogger(EquipoRemotoService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
