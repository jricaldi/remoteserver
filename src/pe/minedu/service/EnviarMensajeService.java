
package pe.minedu.service;

import java.io.Serializable;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class EnviarMensajeService implements Serializable{
    
    public static void mostarMensaje(String mensaje, String docente){
        JOptionPane opt = new JOptionPane(mensaje, JOptionPane.WARNING_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{});
        final JDialog dlg = opt.createDialog(docente);
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(5000);
                    dlg.dispose();
                } catch (Throwable th) {
                }
            }
        }).start();
        dlg.setVisible(true);
        //JOptionPane.showMessageDialog(null, mensaje, docente, JOptionPane.WARNING_MESSAGE);
    }
    
}
