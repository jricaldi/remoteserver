
package pe.minedu.service;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import javax.swing.JOptionPane;
import pe.minedu.bean.RemoteServerBean;
import pe.minedu.util.Constantes;
import pe.minedu.util.FechaUtil;
import pe.minedu.util.FileUtil;

public class AbrirArchivoService implements Serializable{

    public AbrirArchivoService() {
    }
    
    /**
     * el atributo RemoteServerBean.documento debe de ser la ruta completa del archivo en el servidor, 
     * ejemplo 192.168.1.100\sesiones\Doc1.docx, ya que este archivo se va a descargar en la maquina del estudiante
     * @param remoteServerBean Objeto con los atributos necesarios para ejecutar el comando
     * @throws Exception
     * */
    public void abrirArchivoSesion(RemoteServerBean remoteServerBean) throws Exception{
        try{
        String carpeta = "";
        if(remoteServerBean.getPerfil().equals(Constantes.PERFIL_ESTUDIANTE)){
            carpeta += Constantes.ESTUDIANTE_PATH;
        } else if(remoteServerBean.getPerfil().equals(Constantes.PERFIL_DOCENTE)){
            carpeta += Constantes.DOCENTE_PATH;
        }
        carpeta += File.separator + generarCarpetaLocal(remoteServerBean);
        FileUtil.crearCarpeta(carpeta);
        String arbrirArchivoSesion = FileUtil.descargarArchivo(remoteServerBean.getDocumento(), carpeta);   
        File file = new File(arbrirArchivoSesion);
        if(!file.exists()){
            throw new Exception("Archivo no Encontrado, asegurese que el documento se encuentro en la Base.");
        }        
        System.out.println("arbrirArchivoSesion " + arbrirArchivoSesion);
        abrirArchivoCommand(arbrirArchivoSesion);        
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, ex.getMessage());
            ex.printStackTrace();
        }
    }
    
    /**
     * el atributo RemoteServerBean.documento debe de ser solo el nombre del archivo a crear por ejemplo Doc1.docx
     * @param remoteServerBean Objeto con los atributos necesarios para ejecutar el comando
     * @throws Exception
     * */
    public void abrirArchivoNuevo(RemoteServerBean remoteServerBean) throws Exception{
        try{
            String carpeta = "";
            if(remoteServerBean.getPerfil().equals(Constantes.PERFIL_ESTUDIANTE)){
                carpeta += Constantes.ESTUDIANTE_PATH;
            } else if(remoteServerBean.getPerfil().equals(Constantes.PERFIL_DOCENTE)){
                carpeta += Constantes.DOCENTE_PATH;
            }
            File file = new File(carpeta);        
            if(!file.exists()){
                file.mkdirs();
            }            
            carpeta += File.separator + generarCarpetaLocal(remoteServerBean);
            FileUtil.crearCarpeta(carpeta);
            String archivoNuevo = carpeta + File.separator + remoteServerBean.getDocumento();
            System.out.println("archivoNuevo " + archivoNuevo);
            FileUtil.crearNuevoArchivo(archivoNuevo);
            abrirArchivoCommand(archivoNuevo);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Algoritmo para crear la carpeta del estudiante [FECHA]_[ALU/DOC]_[COD_USUARIO]_[COD_CLASE]
     * Ejemplo: 20151205_ALU_12585_SESI01
     * @param remoteServerBean 
     * 
     * */
    private String generarCarpetaLocal(RemoteServerBean remoteServerBean){
        String carpeta = FechaUtil.fechaHoyStringIso();
        if(remoteServerBean.getPerfil().equals(Constantes.PERFIL_ESTUDIANTE)){
            carpeta += "_" + Constantes.PREFIJO_ALUMNO;
        }if(remoteServerBean.getPerfil().equals(Constantes.PERFIL_DOCENTE)){
            carpeta += "_" + Constantes.PREFIJO_DOCENTE;
        }
        carpeta += "_" + remoteServerBean.getCodigoUsuario();
        carpeta += "_" + remoteServerBean.getCodigoClase();
        return  carpeta;
    }
    
    /**
     * Comando para abrir un archivo en windows
     * @param ruta ruta del archivo a abrir en la maquina del estudiante
     * @throws IOException
     * @throws InterruptedException
     * */
    private static void abrirArchivoCommand(String ruta) throws IOException, InterruptedException, Exception{
        try{
        Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + ruta);
        p.waitFor();
        }catch(Exception e){
           throw new Exception("No esxiste la ruta");
        }
    }
}
