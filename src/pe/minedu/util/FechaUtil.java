
package pe.minedu.util;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FechaUtil implements Serializable{

    private static Calendar fecha;
    public static String fechaHoyStringIso(){
        fecha = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("YYYYMMdd");
        return dateFormat.format(fecha.getTime());
    }
}
